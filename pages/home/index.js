Page({
    data:
        {
            p1: "微思科技(WeBoost)位于澳大利亚墨尔本，是一家专业IT开发公司。我们的主营业务包括网站开发、微信小程序开发、手机应用开发、搜索引擎优化（SEO）以及企业高级制定系统开发（ERP、CRM等）。",
            type1: "网站开发",
            type2: "搜索排名优化",
            type3: "定制系统开发",
            type4: "微信小程序",
            phone: "0490 825 180 (点击拨打)",
            subtitle: "我们的优势",
            mobile: "03 8510 9548 (点击拨打)",
            address: "635 Waverley Rd, Glen Waverley (导航)",
            mail: "info@weboost.com.au",
            weTitle1: "实力技术后盾",
            wedesc1: "依托于澳洲本地团队多年网站及软件开发运营经验，为不同类型和规模的项目提供专业服务",
            weTitle2: "特色定制服务",
            wedesc2: "紧跟市场需求，为客户研发最具有商业价值的网站和应用，从头至尾倾听客户需求，提供专业建议",
            weTitle3: "交付准时",
            wedesc3: "十多年开发经验的专业开发团队，精准把控整个开发流程，客户可以随时跟进开发进度",
            weTitle4: "一站式服务",
            wedesc4: "全面运营支持、全面营销支7*24小时程序稳定性支持",
        },
    case1: function () {
        wx.navigateTo(
            {
                url: '../website/website'
            }
        )
    },
    case2: function () {
        wx.navigateTo({
            url: '../seo/seo'
        })
    },
    case3: function () {
        wx.navigateTo({
            url: '../wechat/wechat'
        })
    },
    case4: function () {
        wx.navigateTo({
            url: '../system/system'
        })
    },
    onShareAppMessage: function () {
        return {
            title: '微思科技',
            desc: '',
            imageUrl:'../../images/logo.jpg',
            path: '/pages/home/index'
        }
    },
    callPhone: function () {
      wx.makePhoneCall({
        phoneNumber: '0490825180',
        success: function () {
          console.log("拨打电话成功！")
        },
        fail: function () {
          console.log("拨打电话失败！")
        }
      })
    },
    callMobile: function () {
      wx.makePhoneCall({
        phoneNumber: '0385109548',
        success: function () {
          console.log("拨打电话成功！")
        },
        fail: function () {
          console.log("拨打电话失败！")
        }
      })
    },
    click: function (e) {
      wx.openLocation({
        latitude: -37.887698,
        longitude: 145.156524,
        scale: 18,
        name: 'Weboost',
        address: '635 Waverley Rd, Glen Waverley VIC 3150'
      })
    }
})